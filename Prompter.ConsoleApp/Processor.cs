﻿using System;
using System.Linq;
using Microsoft.Extensions.Options;
using Prompter.Infrastructure.Services;

namespace Prompter.ConsoleApp
{
    public class Processor
    {
        private readonly ICommandResolver _commandResolver;
        private readonly ProcessorOptions _processorOptions;
        private readonly IPrompt _prompt;

        public Processor(ICommandResolver commandResolver, IPrompt prompt, IOptions<ProcessorOptions> options)
        {
            _commandResolver = commandResolver;
            _prompt = prompt;
            _processorOptions = options.Value;
        }

        public void Start(string[] args)
        {
            var textCommand = args.FirstOrDefault();
            _commandResolver.ExecuteCommands(textCommand);

            while (true)
            {
                var partWord = Console.ReadLine();

                if (string.IsNullOrEmpty(partWord))
                    break;
                //ToDo: Выход нажатием Esc

                foreach (var world in _prompt.FindFor(partWord, _processorOptions.MaxWordCount))
                    Console.WriteLine(world);
            }
        }
    }
}