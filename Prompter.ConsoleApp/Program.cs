﻿using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Prompter.Infrastructure;
using Prompter.Infrastructure.Models;

namespace Prompter.ConsoleApp
{
    internal class Program
    {
        private const string SETTINGS_JSON = "appsettings.json";
        private static ServiceProvider _serviceProvider;

        private static void Main(string[] args)
        {
            ConfigureServices();

            var processor = _serviceProvider.GetService<Processor>();
            processor.Start(args);
        }

        private static void ConfigureServices()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(SETTINGS_JSON)
                .Build();
            var connectionString = configuration.GetConnectionString(nameof(PrompterContext));
            var processorOptions = configuration.GetSection(nameof(ProcessorOptions));

            _serviceProvider = new ServiceCollection()
                .Configure<ProcessorOptions>(x => processorOptions.Bind(x))
                .AddScoped<Processor>()
                .AddInfrastructureConfig(connectionString)
                .BuildServiceProvider();
        }
    }
}