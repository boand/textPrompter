﻿using System.Threading;
using Microsoft.Extensions.DependencyInjection;
using Prompter.Infrastructure;

namespace Prompter.Server
{
    internal class Program
    {
        private static ServiceProvider _serviceProvider;

        private static void Main(string[] args)
        {
            var connectionString = args[0];
            var port = args[1];

            ConfigureServices(connectionString);
            var serverProcessor = new ServerProcessor(_serviceProvider);
            var processorThread = new Thread(serverProcessor.Start);
            processorThread.Start(port);

            var processor = _serviceProvider.GetService<Processor>();
            processor.Start();
        }

        private static void ConfigureServices(string connectionString)
        {
            _serviceProvider = new ServiceCollection()
                .AddScoped<ServerProcessor>()
                .AddScoped<Processor>()
                .AddInfrastructureConfig(connectionString)
                .BuildServiceProvider();
        }
    }
}