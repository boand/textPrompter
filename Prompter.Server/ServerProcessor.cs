﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.Extensions.DependencyInjection;
using Prompter.Infrastructure.Services;

namespace Prompter.Server
{
    internal class ServerProcessor
    {
        private const string IP_ADDRESS = "127.0.0.1";
        private readonly List<ClientProcess> _clientObjects = new List<ClientProcess>();
        private readonly ServiceProvider _serviceProvider;
        private TcpListener _listener;

        public ServerProcessor(ServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void Start(object portString)
        {
            try
            {
                var port = int.Parse(portString.ToString());
                _listener = new TcpListener(IPAddress.Parse(IP_ADDRESS), port);
                _listener.Start();

                while (true)
                {
                    var client = _listener.AcceptTcpClient();
                    var prompt = _serviceProvider.GetService<IPrompt>();
                    var clientProcess = new ClientProcess(client, prompt);
                    _clientObjects.Add(clientProcess);

                    var clientThread = new Thread(clientProcess.Process);
                    clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                _listener?.Stop();
                foreach (var clientObject in _clientObjects)
                    clientObject.Dispose();
            }
        }
    }
}