﻿using System;
using System.Net.Sockets;
using System.Text;
using Prompter.Infrastructure.Services;

namespace Prompter.Server
{
    public class ClientProcess : IDisposable
    {
        private readonly TcpClient _client;
        private readonly IPrompt _prompt;
        private NetworkStream _stream;

        public ClientProcess(TcpClient client, IPrompt prompt)
        {
            _client = client;
            _prompt = prompt;
        }

        public void Dispose()
        {
            _client?.Dispose();
            _stream?.Dispose();
        }

        public void Process()
        {
            try
            {
                _stream = _client.GetStream();
                while (true)
                {
                    var request = GetRequest(_stream);
                    var response = DoWork(request);
                    SendResponse(_stream, response);
                }
            }
            catch
            {
                // ignored
            }
        }

        private string GetRequest(NetworkStream stream)
        {
            var builder = new StringBuilder();
            do
            {
                var buffer = new byte[64];
                var bytes = stream.Read(buffer, 0, buffer.Length);
                builder.Append(Encoding.Unicode.GetString(buffer, 0, bytes));
            } while (stream.DataAvailable);

            return builder.ToString();
        }

        private void SendResponse(NetworkStream stream, string response)
        {
            var data = Encoding.Unicode.GetBytes(response);
            stream.Write(data, 0, data.Length);
        }

        private string DoWork(string request)
        {
            var partWord = request.Substring(4);
            var builder = new StringBuilder();
            builder.AppendLine("-");

            foreach (var world in _prompt.FindFor(partWord))
                builder.AppendLine(world);

            return builder.ToString();
        }
    }
}