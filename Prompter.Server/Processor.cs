﻿using System;
using Prompter.Infrastructure.Services;

namespace Prompter.Server
{
    internal class Processor
    {
        private readonly ICommandResolver _commandResolver;

        public Processor(ICommandResolver commandResolver)
        {
            _commandResolver = commandResolver;
        }

        public void Start()
        {
            while (true)
            {
                var textCommand = Console.ReadLine();
                _commandResolver.ExecuteCommands(textCommand);
            }
        }
    }
}