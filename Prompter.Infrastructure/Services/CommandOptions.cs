﻿namespace Prompter.Infrastructure.Services
{
    public class CommandOptions
    {
        public int MinWordCount { get; set; }
        public int MinWordLength { get; set; }
        public int MaxWordLength { get; set; }
        public string InputFileName { get; set; }
    }
}