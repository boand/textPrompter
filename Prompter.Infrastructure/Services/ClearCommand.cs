﻿using Prompter.Infrastructure.Models;

namespace Prompter.Infrastructure.Services
{
    public class ClearCommand : ICommand
    {
        private readonly PrompterContext _context;

        public ClearCommand(PrompterContext context)
        {
            _context = context;
        }

        public string TextCommand => "очистить словарь";

        public void Execute()
        {
            _context.Truncate(nameof(_context.Pairs));
        }
    }
}