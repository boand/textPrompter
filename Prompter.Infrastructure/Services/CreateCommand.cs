﻿namespace Prompter.Infrastructure.Services
{
    public class CreateCommand : ICommand
    {
        private readonly ClearCommand _clearCommand;
        private readonly RefreshCommand _refreshCommand;

        public CreateCommand(ClearCommand clearCommand, RefreshCommand refreshCommand)
        {
            _clearCommand = clearCommand;
            _refreshCommand = refreshCommand;
        }


        public string TextCommand => "создание словаря";

        public void Execute()
        {
            _clearCommand.Execute();
            _refreshCommand.Execute();
        }
    }
}