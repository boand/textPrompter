﻿namespace Prompter.Infrastructure.Services
{
    public interface ICommandResolver
    {
        void ExecuteCommands(string textCommand);
    }
}