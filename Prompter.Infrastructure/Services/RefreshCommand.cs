﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Prompter.Infrastructure.Models;

namespace Prompter.Infrastructure.Services
{
    public class RefreshCommand : ICommand
    {
        private readonly CommandOptions _commandOptions;
        private readonly PrompterContext _context;
        private readonly Regex _wordPattern;

        public RefreshCommand(PrompterContext context, IOptions<CommandOptions> options)
        {
            _context = context;
            _commandOptions = options.Value;
            _wordPattern = new Regex($"\\w{{{_commandOptions.MinWordLength},{_commandOptions.MaxWordLength}}}");
        }

        public string TextCommand => "обновление словаря";

        public async void Execute()
        {
            var taskRead = ReadAllTextAsync();

            var existingWords = await _context.Pairs
                .AsNoTracking()
                .ToDictionaryAsync(x => x.World, x => x.Rate);
            var newWords = new Dictionary<string, int>();

            var content = await taskRead;
            foreach (Match match in _wordPattern.Matches(content))
            {
                var word = match.Value.ToLower();
                if (existingWords.TryGetValue(word, out var currentCount))
                {
                    currentCount++;
                    existingWords[word] = currentCount;
                }
                else
                {
                    newWords.TryGetValue(word, out currentCount);
                    currentCount++;
                    newWords[word] = currentCount;
                }
            }

            var wordsToUpdate = existingWords
                .Select(x => new Pair {World = x.Key, Rate = x.Value});
            _context.Pairs.UpdateRange(wordsToUpdate);

            var wordsToInsert = newWords
                .Where(x => x.Value >= _commandOptions.MinWordCount)
                .Select(x => new Pair {World = x.Key, Rate = x.Value});
            _context.Pairs.AddRange(wordsToInsert);
            await _context.SaveChangesAsync();
        }

        private async Task<string> ReadAllTextAsync()
        {
            using (var reader = File.OpenText(_commandOptions.InputFileName))
            {
                return await reader.ReadToEndAsync();
            }
        }
    }
}