﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Prompter.Infrastructure.Models;

namespace Prompter.Infrastructure.Services
{
    public class Prompt : IPrompt
    {
        private readonly PrompterContext _context;

        public Prompt(PrompterContext context)
        {
            _context = context;
        }

        public string[] FindFor(string partWord, int take = 5)
        {
            var result = _context.Pairs
                .AsNoTracking()
                .Where(x => x.World.StartsWith(partWord))
                .OrderByDescending(x => x.Rate)
                .ThenBy(x => x.World)
                .Take(take)
                .Select(x => x.World);

            return result.ToArray();
        }
    }
}