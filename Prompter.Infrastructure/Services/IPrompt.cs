﻿namespace Prompter.Infrastructure.Services
{
    public interface IPrompt
    {
        string[] FindFor(string partWord, int take = 5);
    }
}