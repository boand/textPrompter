﻿namespace Prompter.Infrastructure.Services
{
    public interface ICommand
    {
        string TextCommand { get; }
        void Execute();
    }
}