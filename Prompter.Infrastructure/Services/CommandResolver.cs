﻿using System.Collections.Generic;
using System.Linq;

namespace Prompter.Infrastructure.Services
{
    public class CommandResolver : ICommandResolver
    {
        private readonly IEnumerable<ICommand> _commands;

        public CommandResolver(IEnumerable<ICommand> commands)
        {
            _commands = commands;
        }

        public void ExecuteCommands(string textCommand)
        {
            if (string.IsNullOrEmpty(textCommand))
                return;

            var command = _commands.FirstOrDefault(c => c.TextCommand == textCommand);
            command?.Execute();
        }
    }
}