﻿using System.ComponentModel.DataAnnotations;

namespace Prompter.Infrastructure.Models
{
    public class Pair
    {
        [Key] [MaxLength(15)] public string World { get; set; }

        public int Rate { get; set; }
    }
}