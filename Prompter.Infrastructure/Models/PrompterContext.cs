﻿using Microsoft.EntityFrameworkCore;

namespace Prompter.Infrastructure.Models
{
    public class PrompterContext : DbContext
    {
        public PrompterContext(DbContextOptions<PrompterContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Pair> Pairs { get; set; }

        public void Truncate(string tableName)
        {
            Database.ExecuteSqlRaw($"TRUNCATE TABLE [{tableName}]");
        }
    }
}