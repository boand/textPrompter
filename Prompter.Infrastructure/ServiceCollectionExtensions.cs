﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Prompter.Infrastructure.Models;
using Prompter.Infrastructure.Services;

namespace Prompter.Infrastructure
{
    public static class ServiceCollectionExtensions
    {
        private const string SETTINGS_JSON = "infrastructureSettings.json";

        public static IServiceCollection AddInfrastructureConfig(this IServiceCollection services,
            string connectionString)
        {
            services.AddDbContext<PrompterContext>(options => options.UseSqlServer(connectionString));
            services.AddScoped<IPrompt, Prompt>();

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(SETTINGS_JSON)
                .Build();
            var commandOptions = configuration.GetSection(nameof(CommandOptions));

            services.AddTransient<ICommandResolver, CommandResolver>();
            services.Configure<CommandOptions>(x => commandOptions.Bind(x));

            services.AddTransient<CreateCommand>();
            services.AddTransient<RefreshCommand>();
            services.AddTransient<ClearCommand>();
            services.AddTransient<ICommand, CreateCommand>();
            services.AddTransient<ICommand, RefreshCommand>();
            services.AddTransient<ICommand, ClearCommand>();

            return services;
        }
    }
}