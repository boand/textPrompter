﻿using System;
using System.Net.Sockets;
using System.Text;

namespace Prompter.Client
{
    internal class ClientProcessor : IDisposable
    {
        private readonly TcpClient _client;
        private readonly string _requestFormat;
        private readonly NetworkStream _stream;

        public ClientProcessor(string[] args, string requestFormat)
        {
            _requestFormat = requestFormat;
            _client = new TcpClient(args[0], int.Parse(args[1]));
            _stream = _client.GetStream();
        }

        public void Dispose()
        {
            _client?.Dispose();
            _stream?.Dispose();
        }

        public void Start()
        {
            while (true)
            {
                var partWord = Console.ReadLine();
                if (string.IsNullOrEmpty(partWord))
                    break;

                var request = string.Format(_requestFormat, partWord);
                SendRequest(request);

                var response = GetResponse();
                Console.WriteLine(response);
            }
        }

        private void SendRequest(string request)
        {
            var data = Encoding.Unicode.GetBytes(request);
            _stream.Write(data, 0, data.Length);
        }

        private string GetResponse()
        {
            var builder = new StringBuilder();
            do
            {
                var buffer = new byte[64];
                var bytes = _stream.Read(buffer, 0, buffer.Length);
                builder.Append(Encoding.Unicode.GetString(buffer, 0, bytes));
            } while (_stream.DataAvailable);

            return builder.ToString();
        }
    }
}