﻿using System;

namespace Prompter.Client
{
    internal class Program
    {
        private const string REQUEST_FORMAT = "get {0}";

        private static void Main(string[] args)
        {
            try
            {
                using var clientProcessor = new ClientProcessor(args, REQUEST_FORMAT);
                clientProcessor.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}